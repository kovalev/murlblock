mUrlBlock - blocking urls via a modem
=====================================

Copyright (c) 2012-2013, Alexander Ray Kovalev

E-mail: ray.kovalev@gmail.com


Description
-----------
Пгоргамма для быстрого доступа и блокировки/разблокировки url'ов на модемах M-200A/M-200B
от "Промсвязь", раздаваемых БелТелеКом.
