#!/usr/bin/env python
# -*- coding: UTF-8 -*-

#-----------------------------------------------------------------------------
# Author:     Alexander Ray Kovalev
#
# Copyright:  (c) 2012 Alexander Kovalev
# E-mail:     ray.kovalev@gmail.com
#-----------------------------------------------------------------------------

import shelve
import sys
import telnetlib
import gui
from PySide import QtCore, QtGui

class url_n_block(QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = gui.Ui_UrlBlock()
        self.ui.setupUi(self)

        self.check_names_list = [
            'check_0', 'check_1', 'check_2', 'check_3', 
            'check_4', 'check_5', 'check_6', 'check_7',
            'check_8', 'check_9', 'check_10','check_11',
            'check_12','check_13','check_14','check_15',
        ]

        self.url_values_list = [
                    'url_n_0', 'url_n_1', 'url_n_2', 'url_n_3', 
                    'url_n_4', 'url_n_5', 'url_n_6', 'url_n_7',
                    'url_n_8', 'url_n_9', 'url_n_10','url_n_11',
                    'url_n_12','url_n_13','url_n_14','url_n_15',
        ]

        urls = shelve.open('urls')
        try:
            self.ui.ip_address.setText(urls['modem_ip'])
        except:
            urls['modem_ip'] = ''
            self.ui.ip_address.setText('')
        rng = range(16)
        for i in rng:
            try:
                param_value = str(i) + '_value'
                param_status = str(i) + '_status'

                eval('self.ui.' + self.url_values_list[i] + '.setText("' + urls[param_value] + '")')
                eval('self.ui.' + self.check_names_list[i] + '.setChecked(' + urls[param_status] + ')')
            except:
                eval('self.ui.' + self.url_values_list[i] + '.setText(\'\')')
                urls[param_value] = ''
                urls[param_status] = 'False'

        urls.close()

        self.ui.actionBlockAll.clicked.connect(self.apply_block)
        self.ui.actionKillAll.clicked.connect(self.kill_all)
        
    # self.ui.b0.clicked.connect(lambda: self.block_url('0', self.ui.url_n_0.text()))

    # Clear filter list in modem
    def kill_all(self):
        if self.T_login():
            self.T_execute('sys filter set url enable')
            rng = range(16)
            for j in rng:
                self.T_execute('sys filter set url del ' + str(j))
            
            self.user_message('Линки успешно прибиты!')
            self.T_logout()
        else:
            self.T_logout()

    # Apply filter changes
    #<-----------------------------------------------------------------------------
    def apply_block(self):
        if self.T_login():
            self.T_execute('sys filter set url enable')
            urls = shelve.open('urls')
            rng = range(16)

            # Clear filter list in modem
            for j in rng:
                self.T_execute('sys filter set url del ' + str(j))

            for i in rng:
                cur_url_status = eval('self.ui.' + self.check_names_list[i] + '.checkState()')
                cur_url_value = eval('self.ui.' + self.url_values_list[i] + '.text()')
                
                param_value = str(i) + '_value'
                param_status = str(i) + '_status'

                urls[param_value] = cur_url_value
                if cur_url_status:
                    urls[param_status] = 'True'
                    self.T_execute('sys filter set url add ' + str(i) + ' ' + str(cur_url_value))
                else:
                    urls[param_status] = 'False'

            urls.close()
            self.T_logout()
            self.user_message('Данные успешно обновлены!')
        else:
            self.T_logout()

    def wrong_pass(self):
        QtGui.QMessageBox.warning(self, 'Внимание!'.decode('UTF-8'), 'Неверный пароль'.decode('UTF-8'), QtGui.QMessageBox.Ok)

    def user_message(self, text):
        QtGui.QMessageBox.information(self, 'Уведомление'.decode('UTF-8'), text.decode('UTF-8'), QtGui.QMessageBox.Ok)

    # Telnet commands
    #<-----------------------------------------------------------------------------
    def T_login(self):
        self.host = str(self.ui.ip_address.text())
        if self.host == '':
            self.user_message('Введите IP модема!')
            return False
        self.modem_pass = str(self.ui.pass_line.text())
        self.hT = telnetlib.Telnet(self.host)
        self.hT.read_until('Password: ', 1)
        self.hT.write(self.modem_pass + '\n')
        got_prompt = self.hT.read_until('ZTE>', 1)
        if got_prompt[-4:] != 'ZTE>':
            self.wrong_pass()
            return False
        
        urls = shelve.open('urls')
        urls['modem_ip'] = self.host
        urls.close()
        return True

    def T_logout(self):
        self.hT.write('exit\n')
        self.hT.close()

    def T_execute(self, command):
        self.hT.write(command + '\n')

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    UrlBlock = url_n_block()
    UrlBlock.show()
    sys.exit(app.exec_())

