#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from PySide import QtCore, QtGui


class Ui_UrlBlock(object):
    def setupUi(self, UrlBlock):
        UrlBlock.setObjectName("UrlBlock")
        UrlBlock.resize(577, 269)
        UrlBlock.setMinimumSize(QtCore.QSize(577, 269))
        UrlBlock.setMaximumSize(QtCore.QSize(577, 269))
        self.pass_line = QtGui.QLineEdit(UrlBlock)
        self.pass_line.setGeometry(QtCore.QRect(28, 33, 146, 24))
        self.pass_line.setAutoFillBackground(False)
        self.pass_line.setInputMethodHints(QtCore.Qt.ImhHiddenText|QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhNoPredictiveText)
        self.pass_line.setEchoMode(QtGui.QLineEdit.Password)
        self.pass_line.setAlignment(QtCore.Qt.AlignCenter)
        self.pass_line.setObjectName("pass_line")
        self.pass_label = QtGui.QLabel(UrlBlock)
        self.pass_label.setGeometry(QtCore.QRect(30, 15, 110, 16))
        self.pass_label.setObjectName("pass_label")
        self.ip_address = QtGui.QLineEdit(UrlBlock)
        self.ip_address.setGeometry(QtCore.QRect(218, 33, 146, 24))
        self.ip_address.setAlignment(QtCore.Qt.AlignCenter)
        self.ip_address.setObjectName("ip_address")
        self.ip_label = QtGui.QLabel(UrlBlock)
        self.ip_label.setGeometry(QtCore.QRect(220, 16, 64, 16))
        self.ip_label.setObjectName("ip_label")
        self.actionBlockAll = QtGui.QPushButton(UrlBlock)
        self.actionBlockAll.setGeometry(QtCore.QRect(430, 227, 115, 27))
        self.actionBlockAll.setObjectName("actionBlockAll")
        self.line = QtGui.QFrame(UrlBlock)
        self.line.setGeometry(QtCore.QRect(10, 67, 553, 3))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.layoutWidget = QtGui.QWidget(UrlBlock)
        self.layoutWidget.setGeometry(QtCore.QRect(196, 77, 181, 146))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout_2 = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.check_5 = QtGui.QCheckBox(self.layoutWidget)
        self.check_5.setText("")
        self.check_5.setObjectName("check_5")
        self.gridLayout_2.addWidget(self.check_5, 0, 0, 1, 1)
        self.url_n_5 = QtGui.QLineEdit(self.layoutWidget)
        self.url_n_5.setObjectName("url_n_5")
        self.gridLayout_2.addWidget(self.url_n_5, 0, 1, 1, 1)
        self.check_6 = QtGui.QCheckBox(self.layoutWidget)
        self.check_6.setText("")
        self.check_6.setObjectName("check_6")
        self.gridLayout_2.addWidget(self.check_6, 1, 0, 1, 1)
        self.url_n_6 = QtGui.QLineEdit(self.layoutWidget)
        self.url_n_6.setObjectName("url_n_6")
        self.gridLayout_2.addWidget(self.url_n_6, 1, 1, 1, 1)
        self.check_7 = QtGui.QCheckBox(self.layoutWidget)
        self.check_7.setText("")
        self.check_7.setObjectName("check_7")
        self.gridLayout_2.addWidget(self.check_7, 2, 0, 1, 1)
        self.url_n_7 = QtGui.QLineEdit(self.layoutWidget)
        self.url_n_7.setObjectName("url_n_7")
        self.gridLayout_2.addWidget(self.url_n_7, 2, 1, 1, 1)
        self.check_8 = QtGui.QCheckBox(self.layoutWidget)
        self.check_8.setText("")
        self.check_8.setObjectName("check_8")
        self.gridLayout_2.addWidget(self.check_8, 3, 0, 1, 1)
        self.url_n_8 = QtGui.QLineEdit(self.layoutWidget)
        self.url_n_8.setObjectName("url_n_8")
        self.gridLayout_2.addWidget(self.url_n_8, 3, 1, 1, 1)
        self.check_9 = QtGui.QCheckBox(self.layoutWidget)
        self.check_9.setText("")
        self.check_9.setObjectName("check_9")
        self.gridLayout_2.addWidget(self.check_9, 4, 0, 1, 1)
        self.url_n_9 = QtGui.QLineEdit(self.layoutWidget)
        self.url_n_9.setObjectName("url_n_9")
        self.gridLayout_2.addWidget(self.url_n_9, 4, 1, 1, 1)
        self.layoutWidget_2 = QtGui.QWidget(UrlBlock)
        self.layoutWidget_2.setGeometry(QtCore.QRect(384, 77, 181, 146))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.gridLayout_3 = QtGui.QGridLayout(self.layoutWidget_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.check_10 = QtGui.QCheckBox(self.layoutWidget_2)
        self.check_10.setText("")
        self.check_10.setObjectName("check_10")
        self.gridLayout_3.addWidget(self.check_10, 0, 0, 1, 1)
        self.url_n_10 = QtGui.QLineEdit(self.layoutWidget_2)
        self.url_n_10.setObjectName("url_n_10")
        self.gridLayout_3.addWidget(self.url_n_10, 0, 1, 1, 1)
        self.check_11 = QtGui.QCheckBox(self.layoutWidget_2)
        self.check_11.setText("")
        self.check_11.setObjectName("check_11")
        self.gridLayout_3.addWidget(self.check_11, 1, 0, 1, 1)
        self.url_n_11 = QtGui.QLineEdit(self.layoutWidget_2)
        self.url_n_11.setObjectName("url_n_11")
        self.gridLayout_3.addWidget(self.url_n_11, 1, 1, 1, 1)
        self.check_12 = QtGui.QCheckBox(self.layoutWidget_2)
        self.check_12.setText("")
        self.check_12.setObjectName("check_12")
        self.gridLayout_3.addWidget(self.check_12, 2, 0, 1, 1)
        self.url_n_12 = QtGui.QLineEdit(self.layoutWidget_2)
        self.url_n_12.setObjectName("url_n_12")
        self.gridLayout_3.addWidget(self.url_n_12, 2, 1, 1, 1)
        self.check_13 = QtGui.QCheckBox(self.layoutWidget_2)
        self.check_13.setText("")
        self.check_13.setObjectName("check_13")
        self.gridLayout_3.addWidget(self.check_13, 3, 0, 1, 1)
        self.url_n_13 = QtGui.QLineEdit(self.layoutWidget_2)
        self.url_n_13.setObjectName("url_n_13")
        self.gridLayout_3.addWidget(self.url_n_13, 3, 1, 1, 1)
        self.check_14 = QtGui.QCheckBox(self.layoutWidget_2)
        self.check_14.setText("")
        self.check_14.setObjectName("check_14")
        self.gridLayout_3.addWidget(self.check_14, 4, 0, 1, 1)
        self.url_n_14 = QtGui.QLineEdit(self.layoutWidget_2)
        self.url_n_14.setObjectName("url_n_14")
        self.gridLayout_3.addWidget(self.url_n_14, 4, 1, 1, 1)
        self.layoutWidget_3 = QtGui.QWidget(UrlBlock)
        self.layoutWidget_3.setGeometry(QtCore.QRect(8, 77, 181, 146))
        self.layoutWidget_3.setObjectName("layoutWidget_3")
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget_3)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.check_0 = QtGui.QCheckBox(self.layoutWidget_3)
        self.check_0.setText("")
        self.check_0.setObjectName("check_0")
        self.gridLayout.addWidget(self.check_0, 0, 0, 1, 1)
        self.url_n_0 = QtGui.QLineEdit(self.layoutWidget_3)
        self.url_n_0.setObjectName("url_n_0")
        self.gridLayout.addWidget(self.url_n_0, 0, 1, 1, 1)
        self.check_1 = QtGui.QCheckBox(self.layoutWidget_3)
        self.check_1.setText("")
        self.check_1.setObjectName("check_1")
        self.gridLayout.addWidget(self.check_1, 1, 0, 1, 1)
        self.url_n_1 = QtGui.QLineEdit(self.layoutWidget_3)
        self.url_n_1.setObjectName("url_n_1")
        self.gridLayout.addWidget(self.url_n_1, 1, 1, 1, 1)
        self.check_2 = QtGui.QCheckBox(self.layoutWidget_3)
        self.check_2.setText("")
        self.check_2.setObjectName("check_2")
        self.gridLayout.addWidget(self.check_2, 2, 0, 1, 1)
        self.url_n_2 = QtGui.QLineEdit(self.layoutWidget_3)
        self.url_n_2.setObjectName("url_n_2")
        self.gridLayout.addWidget(self.url_n_2, 2, 1, 1, 1)
        self.check_3 = QtGui.QCheckBox(self.layoutWidget_3)
        self.check_3.setText("")
        self.check_3.setObjectName("check_3")
        self.gridLayout.addWidget(self.check_3, 3, 0, 1, 1)
        self.url_n_3 = QtGui.QLineEdit(self.layoutWidget_3)
        self.url_n_3.setObjectName("url_n_3")
        self.gridLayout.addWidget(self.url_n_3, 3, 1, 1, 1)
        self.check_4 = QtGui.QCheckBox(self.layoutWidget_3)
        self.check_4.setText("")
        self.check_4.setObjectName("check_4")
        self.gridLayout.addWidget(self.check_4, 4, 0, 1, 1)
        self.url_n_4 = QtGui.QLineEdit(self.layoutWidget_3)
        self.url_n_4.setObjectName("url_n_4")
        self.gridLayout.addWidget(self.url_n_4, 4, 1, 1, 1)
        self.layoutWidget1 = QtGui.QWidget(UrlBlock)
        self.layoutWidget1.setGeometry(QtCore.QRect(8, 228, 181, 26))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.gridLayout_4 = QtGui.QGridLayout(self.layoutWidget1)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.check_15 = QtGui.QCheckBox(self.layoutWidget1)
        self.check_15.setText("")
        self.check_15.setObjectName("check_15")
        self.gridLayout_4.addWidget(self.check_15, 0, 0, 1, 1)
        self.url_n_15 = QtGui.QLineEdit(self.layoutWidget1)
        self.url_n_15.setObjectName("url_n_15")
        self.gridLayout_4.addWidget(self.url_n_15, 0, 1, 1, 1)
        self.actionKillAll = QtGui.QPushButton(UrlBlock)
        self.actionKillAll.setGeometry(QtCore.QRect(240, 227, 115, 27))
        self.actionKillAll.setObjectName("actionKillAll")

        self.retranslateUi(UrlBlock)
        QtCore.QMetaObject.connectSlotsByName(UrlBlock)
        UrlBlock.setTabOrder(self.pass_line, self.ip_address)
        UrlBlock.setTabOrder(self.ip_address, self.url_n_0)
        UrlBlock.setTabOrder(self.url_n_0, self.url_n_1)
        UrlBlock.setTabOrder(self.url_n_1, self.url_n_2)
        UrlBlock.setTabOrder(self.url_n_2, self.url_n_3)
        UrlBlock.setTabOrder(self.url_n_3, self.url_n_4)
        UrlBlock.setTabOrder(self.url_n_4, self.url_n_15)
        UrlBlock.setTabOrder(self.url_n_15, self.url_n_5)
        UrlBlock.setTabOrder(self.url_n_5, self.url_n_6)
        UrlBlock.setTabOrder(self.url_n_6, self.url_n_7)
        UrlBlock.setTabOrder(self.url_n_7, self.url_n_8)
        UrlBlock.setTabOrder(self.url_n_8, self.url_n_9)
        UrlBlock.setTabOrder(self.url_n_9, self.url_n_10)
        UrlBlock.setTabOrder(self.url_n_10, self.url_n_11)
        UrlBlock.setTabOrder(self.url_n_11, self.url_n_12)
        UrlBlock.setTabOrder(self.url_n_12, self.url_n_13)
        UrlBlock.setTabOrder(self.url_n_13, self.url_n_14)
        UrlBlock.setTabOrder(self.url_n_14, self.actionBlockAll)
        UrlBlock.setTabOrder(self.actionBlockAll, self.check_0)
        UrlBlock.setTabOrder(self.check_0, self.check_1)
        UrlBlock.setTabOrder(self.check_1, self.check_2)
        UrlBlock.setTabOrder(self.check_2, self.check_3)
        UrlBlock.setTabOrder(self.check_3, self.check_4)
        UrlBlock.setTabOrder(self.check_4, self.check_15)
        UrlBlock.setTabOrder(self.check_15, self.check_5)
        UrlBlock.setTabOrder(self.check_5, self.check_6)
        UrlBlock.setTabOrder(self.check_6, self.check_7)
        UrlBlock.setTabOrder(self.check_7, self.check_8)
        UrlBlock.setTabOrder(self.check_8, self.check_9)
        UrlBlock.setTabOrder(self.check_9, self.check_10)
        UrlBlock.setTabOrder(self.check_10, self.check_11)
        UrlBlock.setTabOrder(self.check_11, self.check_12)
        UrlBlock.setTabOrder(self.check_12, self.check_13)
        UrlBlock.setTabOrder(self.check_13, self.check_14)

    def retranslateUi(self, UrlBlock):
        UrlBlock.setWindowTitle(QtGui.QApplication.translate("UrlBlock", "mUrlBlock", None, QtGui.QApplication.UnicodeUTF8))
        self.pass_label.setText(QtGui.QApplication.translate("UrlBlock", "Пароль модема", None, QtGui.QApplication.UnicodeUTF8))
        self.ip_label.setText(QtGui.QApplication.translate("UrlBlock", "IP модема", None, QtGui.QApplication.UnicodeUTF8))
        self.actionBlockAll.setText(QtGui.QApplication.translate("UrlBlock", "Заблокировать", None, QtGui.QApplication.UnicodeUTF8))
        self.actionKillAll.setText(QtGui.QApplication.translate("UrlBlock", "Разблокировать", None, QtGui.QApplication.UnicodeUTF8))
